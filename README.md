# FZF based shortcuts generator

## Intro

[`fzf`](https://github.com/junegunn/fzf) is a general purpose command line fuzzy finder. I think of it as a general purpose search engine. It's written in [`go`](https://golang.org), has high performance, and is very useful for searching in files, contacts, emails or whatever input you give it.

This program came from the need to define various shortcuts which utilize `fzf` in various programs such as `ranger`, `vim` and the shell. I'll explain with an example:

The most standard usage of `fzf` is when you want to do something similar to this: you have a list you'd like to choose a specific item from, and perhaps perform a specific action on it. Usually, we want to pick a specific directory or file from a large directory and `cd` into it or run / open the file. For example, consider this shell function:

```bash
cdw(){
  local res
  find ~/repos -maxdepth 1 -mindepth 1 -type d | fzf | read -r -d '' res
  cd "$res"
}
```

It pipes a list of the directories in `~/repos` to `fzf`. `fzf` then reads the list and does its black magic, and when the user presses <kbd>enter</kbd>, it prints the choice and reads it into the shell variable `res`. Then this variable is used to `cd` into the chosen directory.

A while after I first started using `fzf` for these kind of shell functions I realised that they share the same structure and only differ by the arguments the `find` command is given and the last command used for the choice made by `fzf`. Initially I could tolerate the repetitiveness of these functions. But after a while, when I realised `fzf` could be used in other interfaces such as [Ranger](http://ranger.github.io/), I thought about using a central configuration file for all of these shortcuts.

For example, defining above command can be done by (Based on a similar example from [Ranger's Wiki](https://github.com/ranger/ranger/wiki/Commands#fzf-integration)):

```python
class cdw(Command):
    """:cdw
    find a directory in the repositories index and cd into it
    """
    def execute(self):
        import subprocess
        import os.path
        command = "find -L /home/doron/repos -mindepth 1 -maxdepth 1 -type d | fzf"
        fzf = self.fm.execute_command(command, universal_newlines=True, stdout=subprocess.PIPE)
        stdout, stderr = fzf.communicate()
        if fzf.returncode == 0:
            fzf_file = os.path.abspath(stdout.rstrip('\n'))
            
            if os.path.isdir(fzf_file):
                self.fm.cd(fzf_file)
            else:
                self.fm.cd(fzf_file)
```

In order to use a single configuration for all of these interfaces, I decided to create `fzfshortcuts` and use a single JSON file along with [Jinja](http://jinja.pocoo.org/) templates to automatically have all my shortcuts defined in a central location for all interfaces, and automatically generate a file I can import in each of those interfaces. Currently, I use `fzfshortcuts` only for Ranger and the Shell.

With the right template, `fzfshortcuts` can be configured to generate `ranger` commands like in the above example using a simple addition to your `commands.py` like this:

```python
# ~/.config/ranger/commands.py
from fzf import *
```

And the same goes for your shell, for example:

```sh
# ~/.bashrc
source ~/.fzf.sh
```

## Installation

```sh
git clone https://gitlab.com/doronbehar/fzfshortcuts
cd fzfshortcuts
python setup.py install
```

## Configuration

Let's start with the main configuration file, located at `$XDG_CONFIG_HOME/fzfshortcuts/config.ini` (`~/.config` is used for `$XDG_CONFIG_HOME` if not set). The default values are shown here:

```ini
[main]
definitions = ~/.config/fzfshortcuts/definitions.json
templates = ~/.config/fzfshortcuts/templates

[save]
```

The `[main]` section has the following keys:

- `definitions`: The path to the JSON file which holds the common parameters to pass to all the templates.
- `templates`: The path to the directory where your Jinja templates are stored.

The `[save]` section doesn't have any default values and defines what templates to look for in the `templates` directory (by filename) and where to save them. For example, consider having a file named `shell` with the following content in your `templates` directory:

```
{% for def in definitions %}
# {{ def.description }}
{{ def.name }} () {
    local res
    {{ def.command }} | fzf | read -r -d '' res
    {{ def.handler }} ${res}
}

{% endfor %}
```

Adding the following to your `[save]` section will make `fzfshortcuts` save the template with the rendered values in `~/.fzf.sh`:

```ini
[save]
shell = ~/.fzf.sh
```

With another template file for `ranger` being put in the `templates` directory, you may want to add the following to your `[save]` section:

```ini
[save]
ranger = ~/.config/ranger/fzf.py
```

So `fzfshortcuts` will save the generated output to `~/.config/ranger/fzf.py`, and you'll be able to use the commands defined in it for ranger by adding this to `~/.config/ranger/commands.py`:

```python
from fzf import *
```

### Notes

Shell variables are expanded, and are written just like in most shells - with `$` before them.

Command line options can alter the values specified in your configuration file.

- Alternative definitions can be chosen with `--definitions` or `-d`.
- An Alternative templates directory can be chosen with `--templates` or `-t`.
- Saving the files as configured in the `[save]` section can be disabled with `--no-save` or `-S` and enabled with `--save` or `-s`.

### Writing templates and definitions

Templates are read by [Jinja](http://jinja.pocoo.org/) so [it's syntax](http://jinja.pocoo.org/docs/2.10/templates/) applies to these template files as well.

Besides that, every template accepts a single array of dictionaries named `definitions` and it may hold any key you'd like to specify in your definitions file.

The `definitions` JSON file should contain a single array of objects with only one mandatory key in every object: `command` which specifies a command string / array of command words / array of arrays of command words. This key is preprocessed before being rendered in the template by `fzfshortcuts` so every argument in the command is quoted properly to fit into a shell command.

Examples are available in the [`examples` directory](https://gitlab.com/doronbehar/fzfshortcuts/tree/master/examples) within the repository. The definitions in the examples are partly taken from my personal definitions. I hope you'll get good inspiration from these, those named `"vig"` and `"odpy"` are among my favorites.

Have fun!

## Command line usage

Other useful command line options

- `-c,--config CONFIG`: For choosing an alternative configuration file.
- `-t,--templates TERMPLATES`: For choosing a specific templates directory.
- `-d,--definitions DEFINITIONS`: For choosing a specific definitions file.
- `-g,--generator GENERATOR`: For rendering a specific template from the templates directory.
- `-v,--verbose`: Increase verbosity.
