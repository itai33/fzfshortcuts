"""A setuptools based setup module.
See:
https://packaging.python.org/en/latest/distributing.html
https://gitlab.com/doronbehar/fzfshortcuts
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
from os import path
# io.open is needed for projects that support Python 2.7
# It ensures open() defaults to text mode with universal newlines,
# and accepts an argument to specify the text encoding
# Python 3 only projects can skip this import
from io import open

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='fzfshortcuts',  # Required
    version='0.0.2',  # Required
    description='FZF based shortcuts generator',  # Optional
    long_description=long_description,  # Optional
    long_description_content_type='text/markdown',  # Optional
    url='https://gitlab.com/doronbehar/fzfshortcuts',  # Optional
    author='Doron Behar',  # Optional
    author_email='me@doronbehar.com',  # Optional
    classifiers=[  # Optional
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
    keywords='fzf templates generator',  # Optional

    # You can just specify package directories manually here if your project is
    # simple. Or you can use find_packages().
    #
    # Alternatively, if you just want to distribute a single Python file, use
    # the `py_modules` argument instead as follows, which will expect a file
    # called `my_module.py` to exist:
    #
    #   py_modules=["my_module"],
    #
    packages=find_packages('src'),  # Required
    package_dir={'fzfshortcuts': "fzfshortcuts"},
    install_requires=['jinja2'],  # Optional
    extras_require={  # Optional
    },
    package_data={  # Optional
        'fzfshortcuts': ['examples'],
    },

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages. See:
    # http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files
    #
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
    data_files=[  # Optional
    ],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # `pip` to create the appropriate form of executable for the target
    # platform.
    #
    # For example, the following would provide a command called `sample` which
    # executes the function `main` from this package when invoked:
    entry_points={  # Optional
        'console_scripts': [
            'fzfshortcuts=fzfshortcuts.cli:main',
        ],
    },
    project_urls={  # Optional
        'Bug Reports': 'https://gitlab.com/doronbehar/fzfshortcuts',
        'Source': 'https://gitlab.com/doronbehar/fzfshortcuts',
    },
)
