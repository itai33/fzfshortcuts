""" Core implementation package.

"""

from .argpconf import Argpconf
from .__version__ import __version__

import logging
argpconf = Argpconf()
_loglevel = argpconf.get_loglevel()
class Logger(logging.Logger):
    def __init__(self, name, level=_loglevel):
        self.name = name
        logging.Logger.__init__(self, name, level)
        self.addHandler(argpconf.get_loghandler())
