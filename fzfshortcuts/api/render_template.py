"""
Render a template according to a set of definitions
"""

import sys
import shlex
import json
import jinja2
from ..core import Logger
logger = Logger(__name__)

def preprocess_command(command):
    """
    Check if command is an array and concatenate every argument given in
    the array with shlex. Allow multiple arrays of command arguments arrays as
    well
    """

    final_command = ""
    if isinstance(command[0], list):
        final_command += "( "
        for cmd in command:
            for arg in cmd:
                final_command += shlex.quote(arg) + " "
            final_command += ";"
        final_command += " )"
    elif isinstance(command, list):
        for arg in command:
            final_command += shlex.quote(arg) + " "
    else:
        final_command = str(command)
    return final_command

def main(template_file, definitions_file, preprocess_command_keys=True):
    """
    render a Jinja template from a file and a definitions file
    """
    # Create template object for template_file
    try:
        template = jinja2.Template(open(template_file).read())
        logger.info("successfully opened template file: %s", template_file)
    except jinja2.exceptions.TemplateSyntaxError as err:
        logger.fatal("a syntax error was found withing template file: %s, exiting",
                     template_file)
        logger.debug(err)
        sys.exit(2)
    except FileNotFoundError:
        logger.fatal("couldn't find template file %s", template_file)
        sys.exit(1)
    # Parse definitions
    try:
        definitions = json.loads(open(definitions_file).read())
        logger.info("successfully opened and loaded JSON definitions from: %s", definitions_file)
    except json.JSONDecodeError as err:
        logger.fatal("a syntax error was found withing JSON definitions file: %s, exiting",
                     definitions_file)
        logger.debug(err)
        sys.exit(2)
    except FileNotFoundError:
        logger.fatal("given definitions file %s was not found", definitions_file)
        sys.exit(1)
    # render the templates through the definitions
    if preprocess_command_keys:
        logger.info("Preprocessing shell commands in definitions")
        for definition in definitions:
            definition['command'] = preprocess_command(definition['command'])
    return template.render(definitions=definitions)
