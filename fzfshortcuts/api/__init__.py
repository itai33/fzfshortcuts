""" Application commands common to all interfaces.

"""
from .render_template import main as render_template

__all__ = "render_template"
